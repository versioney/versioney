import datetime


def parse_date(date: str, format: str = "%Y-%m-%d"):
    return datetime.datetime.strptime(date, format)
