import toml


class Config:
    def __init__(self, path):
        self.__dict__ = toml.load(path)


if __name__ == "__main__":
    c = Config("config.toml")
