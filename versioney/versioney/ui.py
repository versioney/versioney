import click
from pathlib import Path
import crayons
import blindspin
import logging

from .builder import Builder
from .config import Config

log = logging.getLogger(__name__)


@click.group()
@click.option(
    "-v",
    "--verbose",
    default=False,
    is_flag=True,
    help="print more information about what's going on",
)
def cli(verbose):
    loglevel = logging.INFO
    if verbose:
        loglevel = logging.DEBUG
    logging.basicConfig(level=loglevel)


@cli.command(help="create a new site")
@click.option("-p", "--preset", default=None, help="preset to use")
@click.argument("name")
def new(name, preset):
    from .defaults import default_vyfile
    from .versioney import Versioney

    vy = Versioney(Path(name))
    vy.new(preset)
    # print("TODO")
    # print(default_vyfile)


@cli.command(help="build the site")
@click.option(
    "--vyfile",
    default="vy.toml",
    help="location of vyfile",
    type=click.Path(exists=True),
)
@click.option(
    "--verbose", is_flag=True, default=False, help="enable info logs", type=bool
)
def build(vyfile, verbose):
    """
        builds the site.
    """
    log.debug("Building from {}".format(vyfile))
    vyfile = Config(vyfile)
    builder = Builder(vyfile)
    builder.build()


if __name__ == "__main__":
    cli()
