import os
from .utils import parse_date
import shutil
import pystache
import markdown2
import toml
import pprint
from pprint import pformat
import logging
from pathlib import Path

log = logging.getLogger(__name__)

pp = pprint.PrettyPrinter()

# default from markdown2 source
DEFAULT_TAB_WIDTH = 4


class Builder:
    def __init__(self, vyfile):
        """
            Creates the site builder.

            :param vyfile: parsed vyfile (dict)
        """
        self.vyfile = vyfile

        log.debug(
            "template folder is {}".format(
                self.vyfile.build["source_path"] + "templates/"
            )
        )
        self.renderer = pystache.Renderer(
            search_dirs=self.vyfile.build["source_path"] + "templates/"
        )

        self.markdown = markdown2.Markdown(
            html4tags=False,
            tab_width=DEFAULT_TAB_WIDTH,
            safe_mode=None,
            extras=self.vyfile.markdown2["extras"],
            link_patterns=None,
            footnote_title=None,
            footnote_return_symbol=None,
            use_file_vars=False,
        )

        self._to_index = []

    def _gen_buildlist(self):
        """
            generates a list of src/dst pairs to build the site 
        """

        buildlist = []
        path_fmt = self.vyfile.build["source_path"] + "{}/{}"

        # iterate over content types
        for (content_type, options) in self.vyfile.content.items():
            # get all markdown files recursively
            for (_, _, items) in os.walk(
                self.vyfile.build["source_path"] + content_type
            ):

                # add each one to the buildlist
                for item in items:
                    buildlist.append(
                        {
                            "task": "make",
                            "src": path_fmt.format(content_type, item),
                            "dst": path_fmt.format(content_type, item)
                            .replace(
                                self.vyfile.build["source_path"],
                                self.vyfile.build["build_path"],
                            )
                            .replace(".md", ".html"),
                            "content_type": content_type,
                        }
                    )

            if options["index"]:
                log.debug("index will be built for {}".format(content_type))
                buildlist.append(
                    {
                        "task": "index",
                        "src": path_fmt.format(content_type, ""),
                        "dst": path_fmt.format(content_type, "index.html").replace(
                            self.vyfile.build["source_path"],
                            self.vyfile.build["build_path"],
                        ),
                        "content_type": content_type,
                    }
                )

        # add the index to the buildlist
        buildlist.append(
            {
                "task": "make",
                "src": path_fmt.format("", "index.md"),
                "dst": path_fmt.format("", "index.html").replace(
                    self.vyfile.build["source_path"], self.vyfile.build["build_path"]
                ),
            }
        )

        # copy static files
        buildlist.append(
            {
                "task": "copy",
                "src": path_fmt.format("static", ""),
                "dst": path_fmt.format("static", "").replace(
                    self.vyfile.build["source_path"], self.vyfile.build["build_path"]
                ),
            }
        )
        log.debug("buildlist: \n{}".format(pformat(buildlist)))
        self.buildlist = buildlist

    def _make(self, **kwargs):

        log.debug("making {}".format(kwargs["src"]))
        # generate the html for the main content and extract metadata
        with open(kwargs["src"]) as md_file:
            generated = self.markdown.convert(md_file.read())

        # markdown_renderer = markdown_path(
        #     kwargs["src"], extras=self.vyfile.markdown2["extras"]
        # )

        # generated = markdown_renderer.convert()
        # log.debug("markdown title: {}".format(markdown2.__dict__) )
        # check if it has to be added to any index
        in_index = False
        try:
            if self.vyfile.content[kwargs["content_type"]]["index"]:
                self._to_index.append({**kwargs, **generated.metadata})
        except KeyError:
            pass

        # get title if yaml is missing.
        try:
            generated.metadata["title"]
        except KeyError:
            try:
                # get title by getting html blocks and filtering by h1
                title_from_md = [
                    x.replace("<h1>", "").replace("</h1>", "")
                    for _, x in self.markdown.html_blocks.items()
                    if "<h1>" in x
                ][0]
                log.debug("got title from markdown {}".format(title_from_md))
                generated.metadata["title"] = title_from_md
            except KeyError:
                log.error("none titles found")
            log.warning("no title for {}!".format(kwargs["src"]))

        # templating shit
        # template_fill = {"content": generated, **generated.metadata, **self.vyfile.__dict__}
        # pp.pprint(self.vyfile.__dict__)
        template_fill = {
            "content": generated,
            **generated.metadata,
            "vyfile": self.vyfile.__dict__,
        }
        log.debug(generated.metadata)
        # try to load template name:
        # from frontmatter,
        # from vyfile under templates with name matching content_type
        # load default one in [templates]
        try:
            template_name = self.vyfile.templates["default"]
        except KeyError:
            pass
        try:
            template_name = self.vyfile.templates[kwargs["content_type"]]
        except KeyError:
            pass
        try:
            template_name = generated.metadata["template"]
        except KeyError:
            pass

        log.debug("using template {}".format(template_name))

        template_path = self.vyfile.build["source_path"] + "templates/" + template_name

        # fill template
        try:
            with open(template_path) as template:
                rendered = self.renderer.render(template.read(), template_fill)

        except FileNotFoundError as e:
            log.error("template {} not found ".format(template_path))
            exit()

        log.debug("making directories for {}".format(kwargs["dst"]))
        os.makedirs(os.path.dirname(kwargs["dst"]), exist_ok=True)
        with open(kwargs["dst"], "w") as f:
            log.debug("writing {}".format(kwargs["dst"]))
            f.write(rendered)

    def _template_name(self, index=False, **kwargs):

        # add _index to get index templates
        if index:
            kwargs["content_type"] += "_index"

        try:
            return kwargs["generated"].metadata["template"]
        except KeyError:
            log.debug("no template specified in metadata")
            pass
        try:
            return self.vyfile.templates[kwargs["content_type"]]
        except KeyError:
            pass
        try:
            default_template = "default_index" if index else "default"
            return self.vyfile.templates[default_template]
        except KeyError:
            pass
        raise Exception("no template found!")

    def _index(self, **kwargs):
        log.debug("building index for {}".format(kwargs["content_type"]))
        # only get relevant items
        items = {
            "items": [
                x for x in self._to_index if x["content_type"] is kwargs["content_type"]
            ]
        }

        # sorting by date
        try:
            items["items"] = [
                x for x in sorted(items["items"], key=lambda x: parse_date(x["date"]))
            ]

            if self.vyfile.content[kwargs["content_type"]]["order"] == "desc":
                items["items"].reverse()

        except KeyError:
            log.warning(
                "at least one item has no date in frontmatter! Not sorting anything from the category {}".format(
                    kwargs["content_type"]
                )
            )

        for item in items["items"]:
            item["url"] = self.vyfile.baseurl + item["dst"].replace(
                self.vyfile.build["build_path"], ""
            )

        # content_type has _index added.
        # figuring out template name
        template_name = self._template_name(**kwargs, index=True)
        template_path = self.vyfile.build["source_path"] + "templates/" + template_name

        log.debug("using template located at {}".format(template_path))

        with open(template_path) as template:
            rendered = self.renderer.render(
                template.read(),
                {
                    **items,
                    **self.vyfile.content[kwargs["content_type"]],
                    "vyfile": self.vyfile.__dict__,
                },
            )

        with open(kwargs["dst"], "w") as f:
            log.debug("writing {}".format(kwargs["dst"]))
            f.write(rendered)
        pass

    def _copy(self, **kwargs):
        src, dst = kwargs["src"], kwargs["dst"]
        log.debug("copying {} into {}".format(src, dst))
        if os.path.exists(dst):
            shutil.rmtree(dst)
        shutil.copytree(src, dst)

    def build(self):
        self._gen_buildlist()
        if not os.path.exists(".gitignore"):
            with open(".gitignore", "w") as gitignore:
                log.info(
                    "adding {} to .gitignore".format(self.vyfile.build["build_path"])
                )
                gitignore.write(self.vyfile.build["build_path"])
        else:
            log.warn(
                ".gitignore found.\nadd {} manually to avoid commiting generated site.".format(
                    self.vyfile.build["build_path"]
                )
            )
        for step in self.buildlist:
            getattr(self, "_" + step["task"])(**step)
