default_vyfile = """title = "versioney"
baseurl = "http://localhost:8000/"

[build]
    source_path = "src/"
    build_path = "_build/"

[templates]
    # templates are stored into src/templates.
    # content will use default.mustache as a template by default (duh).
    # indexes, such as /posts/, /topics/ will use default_index.
    # You can add/use other templates by adding them in the template folder.
    # To specify another template, either:
    # - specify its name in the frontmatter of your entry
    # declare one here having the same name as your content type.
    # As an example:
    # posts  = "posts.mustache" # corresponds to src/templates/posts.mustache
    default = "default.mustache"
    default_index = "default_index.mustache"

[content]
    # Create another category here.
    [content.posts]
        # setting index to false or removing it won't build an index for that category.
        # available orders are asc, desc (relies on the date variable at frontmatter), or git (soontm)
        index = true 
        order = "asc" 
    [content.pages]
        index = false
    [content.topics]
        index = true
        order = "asc"

[markdown2]
    # extras for markdown2. 
    extras = ["fenced-code-blocks", 
        "tables", 
        "task_list", 
        "metadata"]
"""
