# versioney

## idées

- trois types d'items:
  - **topic**: Rassemble des entrées sur un sujet précis. On y voit dans la page les délétions et ajouts. L'idée c'est d'avoir le contenu, avec la dernière entrée en vert, indiquant que c'est le plus récent. Les parties supprimées seraient aussi accessibles, cachées et "déroulables". On peut aussi y voir un petit historique tout en bas, avec les commit messages.
  - **pages**: Une page classique, genre about, potos, etc. On peut très bien avoir une page qui est un topic.
  - **billets**: Des blogposts conventionnels, usuellement courts.

## technique

- TECH: langage: ?? 
- CSS: encourage l'utilisation de css classless -> **cependant on a forcément besoin de css particulier pour les updates sur les topics**
- TECH: tout est vcsisé, on part sur git
- TECH: KISS
- CSS: Utliliser du css fonctionnel?
- TECH: Voir pour un champs de recherche en seconde intention?
- TECH: Présence de tags, temps de lecture, date, auteur, blah blah blah (yaml frontmatter). Pas de catégories, ou alors on les fera émergentes.

### langage

Python me semble adapté, y'a tout ce qui faut. Ca pourrait être une bonne idée d'utiliser Go, ça peut marcher.
Concrètement, on a besoin d'un parser markdown et d'une implémentation git..

Partons sur python:latest pour l'instant, essayons de faire un proto qui fonctionne.

## "Compilation"

Regardons comment Precis fait. Je veux pas un truc beaucoup plus compliqué que ça..

Tout part du `mk-site`:
- On récupère les tags:
  - TODO